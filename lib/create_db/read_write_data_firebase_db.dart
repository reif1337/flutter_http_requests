import '../model/contacts.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

String dbAddr =
    "https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app";

List<Contacts> list = <Contacts>[
  Contacts(
      first: "Franz",
      last: "Reichel",
      phone: "1234567",
      email: "franz.reichel@htlstp.ac.at"),
  Contacts(
      first: "Werner",
      last: "Gitschthaler",
      phone: "1234567",
      email: "werner.gitschthaler@htlstp.ac.at"),
  Contacts(
      first: "Otto",
      last: "Reichel",
      phone: "1234567",
      email: "otto.reichel@htlstp.ac.at"),
  Contacts(
      first: "Christop",
      last: "Schreiber",
      phone: "1234567",
      email: "christoph.schrieber@htlstp.ac.at"),
  Contacts(
      first: "Konrad",
      last: "Hammerl",
      phone: "1234567",
      email: "konrad.hammerl@htlstp.ac.at"),
];

Future<void> writeListData() async {
  for (Contacts c in list) {
    await writeSingleData(c);
  }
}

// -------------------------------------------
// PUT Request
// -------------------------------------------

Future<void> writeSingleData(Contacts contact) async {
  var url = Uri.parse("$dbAddr/contacts/${contact.first}.json");

  try {
    var response = await http.put(
      url,
      body: json.encode({
        'first': contact.first,
        'last': contact.last,
        'phone': contact.phone,
        'email': contact.email,
      }),
    );

    if (response.statusCode == 200) {
      print("OK");
    } else {
      print("Error: Statuscode: ${response.statusCode}");
      print("Message: ${response.body}");
    }
  } catch (error) {
    print("Error: $error");
  }
}

// -------------------------------------------
// Get Request
// -------------------------------------------
Future<void> readData() async {
  List<Contacts> newList = [];
  final response = await http.get(Uri.parse("$dbAddr/contacts.json"));

  if (response.statusCode == 200) {
    final Map<String, dynamic> data = json.decode(response.body);
    data.forEach((key, value) {
      Contacts contact = Contacts.fromJson(value);
      newList.add(contact);
    });

    print(newList);
  }
}

Future<void> readSpecificUserData() async {
  final response = await http.get(Uri.parse("$dbAddr/contacts/Hauns.json"));

  if (response.statusCode == 200) {
    final Map<String, dynamic> data = json.decode(response.body);

    print(data);
    print(data.values);
  }
}
