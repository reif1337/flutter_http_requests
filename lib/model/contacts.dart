import 'package:flutter/material.dart';

class Contacts {
  String? first;
  String? last;
  String? phone;
  String? email;

  Contacts({@required this.first,
    @required this.last,
    @required this.phone,
    @required this.email});

  factory Contacts.fromJson(Map<String, dynamic> json) {
    return Contacts(
      first: json['first'],
      last: json['last'],
      phone: json['phone'],
      email: json['email'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "first": this.first,
      "last": this.last,
      "phone": this.phone,
      "email": this.email
    };
  }


  @override
  String toString() {
    return 'Contacts{first: $first, last: $last, phone: $phone, email: $email}';
  }
}
