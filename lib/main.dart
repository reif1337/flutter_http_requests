import 'package:flutter/material.dart';
import 'create_db/read_write_data_firebase_db.dart';
import 'model/contacts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HTTP REQUESTS',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter HTTP REQUESTS'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  //Database Link
  String dbAddr =
      "https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Firebase TEST',
            ),


            ElevatedButton(
              onPressed: () {
                readSpecificUserData();
              },
              child: Text('Read Single Contact'),
            ),
            ElevatedButton(
              onPressed: () {
                readData();
              },
              child: Text('Read All Contacts '),
            ),
            ElevatedButton(
              onPressed: () {
                writeSingleData(Contacts(first: "Hauns", last: "Burger", phone: "1234", email: "h.b@hb.at"));
              },
              child: Text('Write single Data into DB'),
            ),

            ElevatedButton(
              onPressed: () {
                writeListData();
                },
              child: Text('Write List of Contacts'),
            ),

          ],
        ),
      ),
    );
  }
}

//CRUD

//C - PUT
// curl -X PUT -d '{ "first": "Jack", "last": "Sparrow" }' 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts/jack/name.json'


//R - GET
// curl 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts.json'
// curl 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts.json?shallow=true'

//U - PATCH
// curl -X PATCH -d '{ "last": "Vogel" }' 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts/jack/name.json'


//D - DELETE
// curl -X DELETE 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts/jack/name/last.json'
// curl -X DELETE 'https://fluttertestprojectrefr-default-rtdb.europe-west1.firebasedatabase.app/contacts/jack.json'

